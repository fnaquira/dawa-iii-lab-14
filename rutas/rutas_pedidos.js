var mongojs = require('mongojs');
var uri = 'mongodb://localhost:27017/db_facebook';
var db = mongojs(uri, ["pedidos","usuarios","productos"]);

function pedidos_listado(req, res){
	db.pedidos.find().sort({nombre:1}, function(err, records){
		if(err){
			console.log('Error al acceder a la base de datos.');
			return;
		}
		res.render('m_pedidos_listado',{records: records});
	});
}

module.exports = {
	
	listado: function(req, res){
		pedidos_listado(req, res);
	},

	nuevo: function(req, res){
		db.usuarios.find().sort({_id:-1},function(err, usuarios){
			if (err) {
				console.log('Error al acceder a la base de datos.');
				res.end();
				return;
			}
			db.productos.find().sort({_id:-1},function(err, productos){
				if (err) {
					console.log('Error al acceder a la base de datos.');
					res.end();
					return;
				}
				res.render('m_pedidos_nuevo',{usuarios: usuarios,productos: productos});
			});
		});
	},

	grabar_nuevo: function (req, res){
		var xnom = req.body['xnom'];
		var xsue = req.body['xsue'];
		db.pedidos.find().sort({_id:-1}, function(err, records){
			if (err) {
				console.log('Error al acceder a la base de datos.');
				res.end();
				return;
			}
			if(records[0]!=null)
        var xid = records[0]._id + 1;
      else
        var xid = 1;
			db.pedidos.insert({_id:xid, nombre:xnom, sueldo:xsue}, function(){
				pedidos_listado(req,res);
			});
		});
	},

	editar: function(req, res){
		var xid=req.params.xid*1;
		console.log(xid);
		db.pedidos.find({_id:xid}, function(err,records){
			if(err){
				console.log('Error al acceder a la base de datos.');
			res.end();
			return;
			}
			res.render('m_pedidos_editar',{pedido: records[0]});
		});
	},

	grabar_editar: function (req,res){
		var xid = req.body['xid']*1;
		var xnom = req.body['xnom'];
		var xsue = req.body['xsue'];
		db.pedidos.update({_id:xid},{nombre:xnom, sueldo:xsue}, function(){
			pedidos_listado(req,res);
		});
	},

	eliminar: function (req, res){
		var xid=req.params.xid*1;
		db.pedidos.remove({_id:xid}, function(){
			pedidos_listado(req,res);
		});
	}
}