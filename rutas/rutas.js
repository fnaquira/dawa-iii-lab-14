var express = require('express');
var router = express.Router();
var fcategorias = require('./rutas_categorias.js');
var fproductos = require('./rutas_productos.js');
var fpedidos = require('./rutas_pedidos.js');

router.get('/', c_inicio);

//Opciones principales
router.get('/mantenimientos', c_mantenimientos);
router.get('/procesos',c_procesos);
router.get('/reportes',c_reportes);

//Opciones de mantenimiento de categorias
router.get('/m_categorias_listado',fcategorias.listado);
router.get('/m_categorias_nuevo',fcategorias.nuevo);
router.post('/m_categorias_grabar_nuevo',fcategorias.grabar_nuevo);
router.get('/m_categorias_editar/:xid',fcategorias.editar);
router.post('/m_categorias_grabar_editar',fcategorias.grabar_editar);
router.get('/m_categorias_eliminar/:xid',fcategorias.eliminar);

//Opciones de mantenimiento de productos
router.get('/m_productos_listado',fproductos.listado);
router.get('/m_productos_nuevo',fproductos.nuevo);
router.post('/m_productos_grabar_nuevo',fproductos.grabar_nuevo);
router.get('/m_productos_editar/:xid',fproductos.editar);
router.post('/m_productos_grabar_editar',fproductos.grabar_editar);
router.get('/m_productos_eliminar/:xid',fproductos.eliminar);

//Opciones de mantenimiento de pedidos
router.get('/m_pedidos_listado',fpedidos.listado);
router.get('/m_pedidos_nuevo',fpedidos.nuevo);
router.post('/m_pedidos_grabar_nuevo',fpedidos.grabar_nuevo);
router.get('/m_pedidos_editar/:xid',fpedidos.editar);
router.post('/m_pedidos_grabar_editar',fpedidos.grabar_editar);
router.get('/m_pedidos_eliminar/:xid',fpedidos.eliminar);

function c_inicio(req, res){
	res.render('index',{});
}

function c_mantenimientos(req, res){
	res.render('mantenimientos',{});
}

function c_procesos(req, res){
	res.send('Procesos');
}

function c_reportes(req, res){
	res.send('Reportes');
}

module.exports = router;