var mongojs = require('mongojs');
var uri = 'mongodb://localhost:27017/db_facebook';
var db = mongojs(uri, ["categorias"]);

var categorias_listado = function(req,res){
	db.categorias.find().sort({nombre:1}, function(err, records){
		if(err){
			console.log('Error al acceder a la base de datos.');
			return;
		}
		res.render('m_categorias_listado',{records: records});
	});
};

module.exports = {
	listado: function(req, res){
		categorias_listado(req,res);
	},
	nuevo: function(req, res){
		res.render('m_categorias_nuevo',{});
	},
	grabar_nuevo: function (req, res){
		var _this = this;
		var xnom = req.body['xnom'];
		var xabr = req.body['xabr'];
		var ximg = req.body['ximg'];
		db.categorias.find().sort({_id:-1}, function(err, records){
			if (err) {
				console.log('Error al acceder a la base de datos.');
				res.end();
				return;
			}
			if(records.length==0)
				var xid = 1;
			else
				var xid = records[0]._id + 1;
			db.categorias.insert({
				_id:xid,
				nombre:xnom,
				abreviatura: xabr,
				imagen: ximg
			}, function(){
				categorias_listado(req,res);
			});
		});
	},

	editar: function(req, res){
		var xid=req.params.xid;
		console.log(xid);
		db.categorias.find({_id: xid*1}, function(err,records){
			if(err){
				console.log('Error al acceder a la base de datos.');
				res.end();
				return;
			}
			res.render('m_categorias_editar',{categoria: records[0]});
		});
	},

	grabar_editar: function (req,res){
		var xid = req.body['xid'];
		var xnom = req.body['xnom'];
		var xabr = req.body['xabr'];
		var ximg = req.body['ximg'];
		db.categorias.update({_id:xid*1},{
			nombre:xnom,
			abreviatura:xabr,
			imagen:ximg
		}, function(){
			categorias_listado(req,res);
		});
	},

	eliminar: function (req, res){
		var xid=req.params.xid;
		db.categorias.remove({_id:xid*1}, function(){
			categorias_listado(req,res);
		});
	}
}