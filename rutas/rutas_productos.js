var mongojs = require('mongojs');
var uri = 'mongodb://localhost:27017/db_facebook';
var db = mongojs(uri, ["productos",'categorias']);

var productos_listado = function(req,res){
	db.productos.find().sort({nombre:1}, function(err, records){
		if(err){
			console.log('Error al acceder a la base de datos.');
			return;
		}
		console.log(records);
		res.render('m_productos_listado',{records: records});
	});
};

module.exports = {
	listado: function(req, res){
		productos_listado(req,res);
	},
	nuevo: function(req, res){
		db.categorias.find().sort({nombre:1}, function(err, records){
			if(err){
				console.log('Error al acceder a la base de datos.');
				return;
			}
			res.render('m_productos_nuevo',{categorias: records});
		});
	},
	grabar_nuevo: function (req, res){
		var _this = this;
		var xnom = req.body['xnom'];
		var xcat = req.body['xcat'];
		var xpre = req.body['xpre'];
		var ximg = req.body['ximg'];
		db.productos.find().sort({_id:-1}, function(err, records){
			if (err) {
				console.log('Error al acceder a la base de datos.');
				res.end();
				return;
			}
			if(records.length==0)
				var xid = 1;
			else
				var xid = records[0]._id + 1;
			db.productos.insert({
				_id:xid,
				nombre:xnom,
				categoria: xcat,
				precio: xpre,
				imagen: ximg
			}, function(){
				productos_listado(req,res);
			});
		});
	},

	editar: function(req, res){
		var xid=req.params.xid;
		db.categorias.find().sort({nombre:1}, function(err, categorias){
			if(err){
				console.log('Error al acceder a la base de datos.');
				return;
			}
			db.productos.find({_id: xid*1}, function(err,records){
				if(err){
					console.log('Error al acceder a la base de datos.');
					res.end();
					return;
				}
				res.render('m_productos_editar',{producto: records[0],categorias: categorias});
			});
		});
	},

	grabar_editar: function (req,res){
		var xid = req.body['xid'];
		var xnom = req.body['xnom'];
		var xcat = req.body['xcat'];
		var xpre = req.body['xpre'];
		var ximg = req.body['ximg'];
		db.productos.update({_id:xid*1},{
			nombre:xnom,
			categoria:xcat,
			precio:xpre,
			imagen:ximg
		}, function(){
			productos_listado(req,res);
		});
	},

	eliminar: function (req, res){
		var xid=req.params.xid;
		db.productos.remove({_id:xid*1}, function(){
			productos_listado(req,res);
		});
	}
}