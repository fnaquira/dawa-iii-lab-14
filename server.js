var express = require('express');
var app = express();
var rutas = require('./rutas/rutas.js');
var bodyParser = require('body-parser');
var handler = require('./handler');
var config = require('./config');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/',rutas);
app.set('view engine','ejs');


app.get('/webhook',function(req,res){
	if(req.query['hub.verify_token']==config.SECURITY){
		res.send(req.query['hub.challenge']);
	}else{
		res.send('Error');
	}
});

app.post('/webhook',function(req,res){
	req.body.entry.forEach(function(entry){
		entry.messaging.forEach(function(message_event){
			if(message_event.message!=null){
				handler.recived_message(message_event,config.PAGE_ACCESS_TOKEN,config.URL_API);
			}
		});
	});
	res.send('ok');
});

var server = app.listen(3000,function(){
	console.log('Escuchando en el puerto 3000');
});