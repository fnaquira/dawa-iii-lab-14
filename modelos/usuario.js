var mongoose = require('mongoose');

var EsquemaUsuario = mongoose.Schema({
	_id: Number,
	first_name : String,
	last_name: String,
	profile_pic: String,
	gender: String,
},
{versionKey: false});

module.exports = mongoose.model("usuarios",EsquemaUsuario,'usuarios');